\contentsline {chapter}{\numberline {1}序　論}{1}
\contentsline {chapter}{\numberline {2}センサノードにおけるデータ通信の先行研究}{2}
\contentsline {section}{\numberline {2.1}フラッディングの先行研究}{2}
\contentsline {section}{\numberline {2.2}可視光通信の先行研究}{2}
\contentsline {chapter}{\numberline {3}天井照明の光度制御を用いたデータ通信手法}{3}
\contentsline {section}{\numberline {3.1}天井照明の光度制御を用いたデータ通信手法概要}{3}
\contentsline {section}{\numberline {3.2}無線センサノードを用いた照度推移実験}{3}
\contentsline {section}{\numberline {3.3}天井照明の光度制御を用いたデータ通信アルゴリズム}{4}
\contentsline {chapter}{\numberline {4} 天井照明の光度制御を用いたデータ通信における通信速度の高速化}{7}
\contentsline {section}{\numberline {4.1}高速化手法概要}{7}
\contentsline {section}{\numberline {4.2}送信間隔による高速化実験}{7}
\contentsline {section}{\numberline {4.3}送信間隔による高速化実験結果と考察}{7}
\contentsline {section}{\numberline {4.4}照度の多段階調変化による高速化実験}{8}
\contentsline {section}{\numberline {4.5}照度の多段階調変化による高速化実験結果と考察}{8}
\contentsline {chapter}{\numberline {5}評価}{11}
\contentsline {section}{\numberline {5.1}天井照明の光度制御を用いたデータ通信における通信速度の評価}{11}
\contentsline {section}{\numberline {5.2}天井照明の光度制御を用いたデータ通信における誤り率の評価}{11}
\contentsline {chapter}{\numberline {6}結論}{12}
